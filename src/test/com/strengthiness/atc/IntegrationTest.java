package com.strengthiness.atc;

import com.strengthiness.atc.aircraft.Aircraft;
import com.strengthiness.atc.aircraft.Boeing747F;
import com.strengthiness.atc.aircraft.Boeing777;
import com.strengthiness.atc.aircraft.CessnaCitationX;
import com.strengthiness.atc.aircraft.DassaultFalconCargo;
import com.strengthiness.atc.command.BootCommand;
import com.strengthiness.atc.command.DequeueCommand;
import com.strengthiness.atc.command.EnqueueCommand;
import com.strengthiness.atc.controllers.SimpleController;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class IntegrationTest {

    Atc atc;

    @BeforeMethod
    public void setUp() throws Exception {
        atc = new Atc();
    }

    @Test
    public void testBoot() throws Exception {
        boot();
        assertTrue(atc.getController() instanceof SimpleController);
    }

    @Test
    public void testSingleAircraft() throws Exception {
        boot();
        final Boeing747F aircraft = new Boeing747F();
        atc.aqmRequestProcess(new EnqueueCommand(aircraft));
        final DequeueCommand dequeueCommand = new DequeueCommand();
        atc.aqmRequestProcess(dequeueCommand);
        assertEquals(dequeueCommand.getRemovedAircraft(), aircraft);
    }

    @Test
    public void testMultipleAircraft() throws Exception {
        boot();
        final Aircraft a1 = new DassaultFalconCargo();
        final Aircraft a2 = new Boeing747F();
        final Aircraft a3 = new CessnaCitationX();
        final Aircraft a4 = new Boeing777();

        atc.aqmRequestProcess(new EnqueueCommand(a1));
        Thread.sleep(1);
        atc.aqmRequestProcess(new EnqueueCommand(a2));
        Thread.sleep(1);
        atc.aqmRequestProcess(new EnqueueCommand(a3));
        Thread.sleep(1);
        atc.aqmRequestProcess(new EnqueueCommand(a4));

        final DequeueCommand dequeueCommand = new DequeueCommand();
        atc.aqmRequestProcess(dequeueCommand);
        assertEquals(dequeueCommand.getRemovedAircraft(), a4);

        atc.aqmRequestProcess(dequeueCommand);
        assertEquals(dequeueCommand.getRemovedAircraft(), a3);

        atc.aqmRequestProcess(dequeueCommand);
        assertEquals(dequeueCommand.getRemovedAircraft(), a2);

        atc.aqmRequestProcess(dequeueCommand);
        assertEquals(dequeueCommand.getRemovedAircraft(), a1);
    }

    private void boot() {
        final BootCommand bootCommand = new BootCommand(SimpleController.class, atc);
        atc.aqmRequestProcess(bootCommand);
    }
}