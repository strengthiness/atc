package com.strengthiness.atc.controllers;

import com.strengthiness.atc.aircraft.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 */
public class SimpleControllerTest {

    SimpleController control;

    @BeforeMethod
    public void setUp() throws Exception {
        control = new SimpleController();
    }

    @Test
    public void testAddAircraft() throws Exception {
        control.addAircraft(new Boeing747F());
        assertEquals(control.landingQueue.size(), 1);
    }

    @Test
    public void testAddMultipleAircraft() throws Exception {
        control.addAircraft(new Boeing747F());
        control.addAircraft(new Boeing747F());
        assertEquals(control.landingQueue.size(), 2);
    }

    @Test
    public void testRemoveAircraft() throws Exception {
        final Boeing747F a1 = new Boeing747F();
        control.addAircraft(a1);

        final Aircraft aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 0);
        assertEquals(aircraft, a1);
    }

    @Test
    public void testRemoveMultipleSimilarAircraft() throws Exception {
        final Aircraft a1 = new Boeing747F();
        final Aircraft a2 = new Boeing747F();
        final Aircraft a3 = new Boeing747F();
        // NOTE: Sleeps added to make sure there is enough time to ensure different date values
        // The code runs fast enough that 2 aircraft can be added in < 1 ms, which is unrealistic for normal use
        control.addAircraft(a1);
        Thread.sleep(1);
        control.addAircraft(a2);
        Thread.sleep(1);
        control.addAircraft(a3);

        Aircraft aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 2);
        assertEquals(aircraft, a1);

        aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 1);
        assertEquals(aircraft, a2);

        aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 0);
        assertEquals(aircraft, a3);
    }

    @Test
    public void testRemoveMultipleDissimilarAircraft() throws Exception {
        final Aircraft a1 = new DassaultFalconCargo();
        final Aircraft a2 = new Boeing747F();
        final Aircraft a3 = new CessnaCitationX();
        final Aircraft a4 = new Boeing777();
        // NOTE: Sleeps added to make sure there is enough time to ensure different date values
        // The code runs fast enough that 2 aircraft can be added in < 1 ms, which is unrealistic for normal use
        control.addAircraft(a1);
        Thread.sleep(1);
        control.addAircraft(a2);
        Thread.sleep(1);
        control.addAircraft(a3);
        Thread.sleep(1);
        control.addAircraft(a4);

        Aircraft aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 3);
        assertEquals(aircraft, a4);

        aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 2);
        assertEquals(aircraft, a3);

        aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 1);
        assertEquals(aircraft, a2);

        aircraft = control.removeAircraft();
        assertEquals(control.landingQueue.size(), 0);
        assertEquals(aircraft, a1);
    }
}