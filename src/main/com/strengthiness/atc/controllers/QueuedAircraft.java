package com.strengthiness.atc.controllers;

import com.strengthiness.atc.aircraft.Aircraft;

import java.util.Date;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Simple representation for an Aircraft in a Controller queue with the time it was added to the queue.
 */
class QueuedAircraft {
    private final Aircraft aircraft;
    private final Date queueTime;

    public QueuedAircraft(Aircraft aircraft, Date queueTime) {
        this.aircraft = aircraft;
        this.queueTime = queueTime;
    }

    public Aircraft getAircraft() {
        return aircraft;
    }

    public Date getQueueTime() {
        return queueTime;
    }


}
