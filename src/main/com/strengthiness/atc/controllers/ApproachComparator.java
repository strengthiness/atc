package com.strengthiness.atc.controllers;

import com.strengthiness.atc.aircraft.AircraftComparator;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Compares queued Aircraft based on the simple AircraftComparator, falling back to time Aircraft was entered into
 * the queue if aircraft are of similar type and size.
 */
public class ApproachComparator implements Comparator<QueuedAircraft> {

    private final AircraftComparator ac = new AircraftComparator();

    @Override
    public int compare(QueuedAircraft q1, QueuedAircraft q2) {
        final int result = ac.compare(q1.getAircraft(), q2.getAircraft());
        if (result != 0) {
            return result;
        }
        return q1.getQueueTime().compareTo(q2.getQueueTime());
    }

}
