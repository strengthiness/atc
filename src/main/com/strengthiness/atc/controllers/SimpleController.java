package com.strengthiness.atc.controllers;

import com.strengthiness.atc.aircraft.Aircraft;

import java.util.Date;
import java.util.PriorityQueue;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Class to represent a simple Air Traffic Controller
 */
public class SimpleController implements Controller {

    // NOTE: If this needs to be multithreaded, use a PriorityBlockingQueue instead
    final PriorityQueue<QueuedAircraft> landingQueue = new PriorityQueue<>(new ApproachComparator());

    @Override
    public void addAircraft(Aircraft aircraft) {
        // TODO: Might want to ensure the same Aircraft instance can't be added twice
        landingQueue.add(new QueuedAircraft(aircraft, new Date()));
    }

    @Override
    public Aircraft removeAircraft() {
        return landingQueue.poll().getAircraft();
    }

}
