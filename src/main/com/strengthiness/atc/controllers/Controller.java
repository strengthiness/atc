package com.strengthiness.atc.controllers;

import com.strengthiness.atc.aircraft.Aircraft;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 */
public interface Controller {

    void addAircraft(Aircraft aircraft);

    Aircraft removeAircraft();

}
