package com.strengthiness.atc.aircraft;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Defines the type of aircraft, typically what role the aircraft serves.
 */
public enum AircraftType {

    PASSENGER,
    CARGO,

}
