package com.strengthiness.atc.aircraft;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 */
public class Boeing747F implements Aircraft {
    @Override
    public AircraftType getAircraftType() {
        return AircraftType.CARGO;
    }

    @Override
    public AircraftSize getAircraftSize() {
        return AircraftSize.HEAVY;
    }
}
