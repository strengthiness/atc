package com.strengthiness.atc.aircraft;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 */
public class Boeing777 implements Aircraft {
    @Override
    public AircraftType getAircraftType() {
        return AircraftType.PASSENGER;
    }

    @Override
    public AircraftSize getAircraftSize() {
        return AircraftSize.HEAVY;
    }
}
