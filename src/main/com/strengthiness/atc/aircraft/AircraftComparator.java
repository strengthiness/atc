package com.strengthiness.atc.aircraft;

import java.util.Comparator;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Simple comparator based on the following rules:
 * <ol>
 *     <li>Passenger before Cargo</li>
 *     <li>HEAVY Aircraft of the same type before LIGHT Aircraft of the same type</li>
 *     <li>Earlier enqueued Aircraft of the same type and size before later enqueued Aircraft of the same type and size</li>
 * </ol>
 */
public class AircraftComparator implements Comparator<Aircraft> {

    @Override
    public int compare(Aircraft o1, Aircraft o2) {
        // HACK: This depends on natural ordering of the Enum types, which is fragile tight coupling, but will suffice for this demo
        int result = o1.getAircraftType().compareTo(o2.getAircraftType());
        if (result != 0) {
            return result;
        }
        return o1.getAircraftSize().compareTo(o2.getAircraftSize());
    }

}
