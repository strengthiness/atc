package com.strengthiness.atc.aircraft;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Simple interface for Aircraft
 */
public interface Aircraft {

    /**
     * Get AircraftType
     * @return AircraftType
     */
    AircraftType getAircraftType();

    /**
     * Get AircraftSize
     * @return AircraftSize
     */
    AircraftSize getAircraftSize();


}
