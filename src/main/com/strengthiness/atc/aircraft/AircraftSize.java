package com.strengthiness.atc.aircraft;

/**
 * Created with IntelliJ IDEA.
 * User: gregory
 * Date: 9/27/15
 *
 * Defines relative sizes of aircraft.
 *
 */
public enum AircraftSize {

    HEAVY,
    LIGHT,

}
