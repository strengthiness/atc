package com.strengthiness.atc;

import com.strengthiness.atc.command.Command;
import com.strengthiness.atc.controllers.Controller;

/**
 * Main air traffic controller class
 */
public class Atc {

    private Controller controller;

    public void aqmRequestProcess(Command command) {
        command.execute(controller);
    }

    public Controller getController() {
        return controller;
    }

    public void setController(Controller controller) {
        this.controller = controller;
    }
}
