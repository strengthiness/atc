package com.strengthiness.atc.command;

import com.strengthiness.atc.aircraft.Aircraft;
import com.strengthiness.atc.controllers.Controller;

/**
 * Command to dequeue the next Aircraft
 */
public class DequeueCommand extends AbstractCommand {

    private Aircraft removedAircraft;

    @Override
    public void execute(Controller controller) {
        super.execute(controller);
        removedAircraft = controller.removeAircraft();
    }

    public Aircraft getRemovedAircraft() {
        return removedAircraft;
    }
}
