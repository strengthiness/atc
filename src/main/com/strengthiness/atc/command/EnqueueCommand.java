package com.strengthiness.atc.command;

import com.strengthiness.atc.aircraft.Aircraft;
import com.strengthiness.atc.controllers.Controller;

/**
 * Command to enqueue an Aircraft
 */
public class EnqueueCommand extends AbstractCommand {

    private final Aircraft aircraft;

    public EnqueueCommand(Aircraft aircraft) {
        this.aircraft = aircraft;
    }

    @Override
    public void execute(Controller controller) {
        super.execute(controller);
        controller.addAircraft(aircraft);
    }
}
