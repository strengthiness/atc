package com.strengthiness.atc.command;

import com.strengthiness.atc.controllers.Controller;

/**
 * An interface to define available commands
 */
public interface Command {

    /**
     * Execute the command on the given Controller
     * @param controller
     */
    void execute(Controller controller);

}
