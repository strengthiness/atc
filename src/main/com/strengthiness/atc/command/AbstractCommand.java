package com.strengthiness.atc.command;

import com.strengthiness.atc.controllers.Controller;

/**
 * Base abstract class for checking that the Controller has been instantiated
 */
public class AbstractCommand implements Command {

    @Override
    public void execute(Controller controller) {
        if (controller == null) {
            throw new IllegalStateException("Controller has not been instantiated");
        }
    }
}
