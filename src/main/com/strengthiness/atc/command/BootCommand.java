package com.strengthiness.atc.command;

import com.strengthiness.atc.Atc;
import com.strengthiness.atc.controllers.Controller;

/**
 * Command to "boot" a Controller
 */
public class BootCommand implements Command {

    private final Class<? extends Controller> controllerClass;
    private final Atc atc;

    public BootCommand(Class<? extends Controller> controllerClass, Atc atc) {
        this.controllerClass = controllerClass;
        this.atc = atc;
    }

    @Override
    public void execute(Controller controller) {
        try {
            atc.setController(controllerClass.newInstance());
        } catch (InstantiationException | IllegalAccessException e) {
            // TODO: better error handling
            System.err.print("Could not instantiate controller");
            e.printStackTrace();
        }
    }
}
